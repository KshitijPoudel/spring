<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html>

<head>
	<title>Login Customer</title>

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css">

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/add-customer-style.css">
		 
		 
		 <link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/login.css">
</head>

<body>
	<div id="container">
		
	
		<form:form action="saveLogin" modelAttribute="login" method="POST">
		
		<form:hidden path="id"/>
    <h2 style="text-align: center;">Login Form</h2>


  <div class="container">
    <label>UserName</label>
    <input type="text" placeholder="Enter Username" name="username" required>

    <label>Password</label>
    <input type="password" placeholder="Enter Password" name="password" required>
        
    <button type="submit">Login</button>
  </div>


		</form:form>
	
		<div style="clear; both;"></div>
		
		
	
	</div>
	
<p>
			<a href="${pageContext.request.contextPath}/customer/list">Back to List</a>
		</p>
	
</body>

</html>










