 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
 <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title> Login </title>

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/style.css">

	<link type="text/css"
		  rel="stylesheet"
		  href="${pageContext.request.contextPath}/resources/css/add-customer-style.css">
</head>
<body>
<div id = "wrapper">
		<div id ="header">
		<h2>Customer Management</h2>
		</div>
	</div>
	
	<div id  = "container">
		<div id = "content">
		
		<input type= "button" value = "Add Login"
			onclick="window.location.href='showFormForAdd';return false;"
			Class = "add-button"
			/>
		
		<table>
			<tr>
				<th>User Name</th>
				<th>Password</th>
				<th>Action</th>
				
				
			</tr>
			
			<c:forEach var= "tempLogin" items = "${logins}">
			
			<!-- construct an "update" link with login id -->
					<c:url var="updateLink" value="/login/showFormForUpdate">
						<c:param name="loginId" value="${tempLogin.id}" />
					</c:url>	
					
					<!-- construct an "delete" link with login id -->
					<c:url var="deleteLink" value="/login/delete">
						<c:param name="loginId" value="${tempLogin.id}" />
					</c:url>						
			
			<tr>
				<td>${tempLogin.username}</td>
				<td>${tempLogin.password}</td>
				
				<td>
							<!-- display the update link -->
				<a href="${updateLink}">Update</a>
				|
				<a href = "${deleteLink}" onclick = "if(!(confirm('Are you sure you want to remove this customer's username & password?')))return false">Delete</a>
				</td>
				
			</tr>
			
			</c:forEach>
		</table>
		
		</div>
	</div>
	
</body>
</html>