 <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 

<!DOCTYPE html>

<html>
<head>

<link type="text/css"
	rel = "stylesheet"
	href = "${pageContext.request.contextPath }/resources/css/style.css">
<title>Customer's List</title>
</head>
<body>
	<div id = "wrapper">
		<div id ="header">
		<h2 style="text-align: center;">Customer Management</h2>
		</div>
	</div>
	
	<div id  = "container">
		<div id = "content">
		
		<input type= "button" value = "Add Customer"
			onclick="window.location.href='showFormForAdd';return false;"
			Class = "add-button"
			/>
		
		<table>
			<tr>
				<th>Name</th>
				<th>Designation</th>
				<th>Date of Appointment</th>
				<th>Rank</th>
				<th>Email</th>
				<th>Action</th>
				
				
				
			</tr>
			
			<c:forEach var= "tempCustomer" items = "${customers}">
			
			<!-- construct an "update" link with customer id -->
					<c:url var="updateLink" value="/customer/showFormForUpdate">
						<c:param name="customerId" value="${tempCustomer.id}" />
					</c:url>	
					
					<!-- construct an "delete" link with customer id -->
					<c:url var="deleteLink" value="/customer/delete">
						<c:param name="customerId" value="${tempCustomer.id}" />
					</c:url>						
			
			<tr>
				<td>${tempCustomer.name}</td>
				<td>${tempCustomer.designation}</td>
				<td>${tempCustomer.appointed_date}</td>
				<td>${tempCustomer.rank}</td>
				<td>${tempCustomer.email}</td>
				<td>
							<!-- display the update link -->
				<a href="${updateLink}">Update</a>
				|
				<a href = "${deleteLink}" onclick = "if(!(confirm('Are you sure you want to delete this customer?')))return false">Delete</a>
				</td>
				
			</tr>
			
			</c:forEach>
		</table>
		
		</div>
	</div>
	
<c:url var = "logoutUrl" value="/logout"/>
 <form class ="form-inline" action="${logoutUrl}" method="post">
<input type="submit" value="log out" >
<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>
	
</body>
</html>