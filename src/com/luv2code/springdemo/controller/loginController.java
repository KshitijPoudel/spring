  package com.luv2code.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.luv2code.springdemo.entity.Login;
import com.luv2code.springdemo.service.LoginService;

@Controller
@RequestMapping("/login")
public class loginController {
	
	//inject login Service
	@Autowired
	private LoginService loginService;
	
	@RequestMapping("/list")
	public String listLogin(Model theModel){
		
		//get logins from dao
		List<Login> theLogins = loginService.getLogins();
		
		//add the logins to the model
		theModel.addAttribute("logins", theLogins);
		
		return "list-logins";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model TheModel){
		//create model attribute to bind form data
		
		Login thelogin = new Login();
		TheModel.addAttribute("login", thelogin);
		
		return "login-form";
	}
	
	@PostMapping("/saveLogin")
	public String saveLogin(@ModelAttribute("login") Login theLogin){
		
		//save login using service
		loginService.saveLogin(theLogin);
		
		return "redirect:/login/list";
		
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("loginId") int theId, Model theModel){
		
		//get login from the service
		Login theLogin = loginService.getLogin(theId);
		
		//set login as model attribute to pre populate the form
		theModel.addAttribute("login", theLogin);
		
		//send over to our form
		return "login-form";
		
	}
	
	@GetMapping("/delete")
	public String deleteLogin(@RequestParam("loginId") int theId){
		
		//delete the login
		
		loginService.deleteLogin(theId);
		
		
		return "redirect:/login/list";
		
		
	}
	

}
