package com.luv2code.springdemo.controller;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/loginn")
public class UserController {
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String printWelcome(ModelMap model, Principal principal){
		System.out.println("Inside login controller");
		String name = "Rupak";
		model.addAttribute("username", name);
		model.addAttribute("message", "Spring security custom form");
		return "list-customers";
		
	}

	@RequestMapping(value = "/helloo", method = RequestMethod.GET)
	public String login(ModelMap model){
		
		return "login";
		
	}
	
	@RequestMapping(value = "/light", method = RequestMethod.GET)
	public String light(ModelMap model){
		
		return "light.jsp";
		
	}
	@RequestMapping(value = "/loginfailed", method = RequestMethod.GET)
	public String loginerror(ModelMap model){
		model.addAttribute("error", "true");
		return "login";
		
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model){
		
		return "login";
		
	}
	
	@RequestMapping
	public String bati(ModelMap model){
		
		return "light.jsp";
		
	}
	 
	
	
	

	


}
