package com.luv2code.springdemo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.luv2code.springdemo.dao.LoginDAO;
import com.luv2code.springdemo.entity.Login;

@Service
public class LoginServiceIMP implements LoginService {
		//injecting login DAO
	@Autowired
	private LoginDAO loginDAO;
	
	@Override
	@Transactional
	public List<Login> getLogins() {
		
		return loginDAO.getLogins();
	}

	@Override
	@Transactional
	public void saveLogin(Login theLogin) {
	
		loginDAO.saveLogin(theLogin);
	}

	@Override
	@Transactional
	public Login getLogin(int theId) {
		
		return loginDAO.getLogin(theId);
	}

	@Override
	@Transactional
	public void deleteLogin(int theId) {
		
	loginDAO.deleteLogin(theId);
		
	}

}
