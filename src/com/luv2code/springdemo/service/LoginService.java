package com.luv2code.springdemo.service;

import java.util.List;

import com.luv2code.springdemo.entity.Login;

public interface LoginService {
	
	public List<Login> getLogins();

	public void saveLogin(Login theLogin);

	public Login getLogin(int theId);

	public void deleteLogin(int theId);

}

