package com.luv2code.springdemo.dao;

import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import com.luv2code.springdemo.entity.Login;

@Repository
public class LoginDAOImp implements LoginDAO {
	
	//inject session factory
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Login> getLogins() {
		
		//get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		//create the query
		Query <Login> theQuery = currentSession.createQuery("from Login" , Login.class);
		
		//return the result
		List<Login> logins = theQuery.getResultList();
		
		return logins;
	}

	@Override
	public void saveLogin(Login theLogin) {
		// get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		//save into database
		currentSession.saveOrUpdate(theLogin);
		
	}
	
	@Override
	public Login getLogin(int theId) {
		//get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		//read data from database using primary key
		Login theLogin = currentSession.get(Login.class, theId);
		return theLogin;
	}

	@Override
	public void deleteLogin(int theId) {
		//get the current hibernate session
		Session currentSession = sessionFactory.getCurrentSession();
		
		//delete object with primary key
		Query<?> theQuery = currentSession.createQuery("delete from Login where id = :loginId");
		theQuery.setParameter("loginId", theId);
		theQuery.executeUpdate();
	}


}
